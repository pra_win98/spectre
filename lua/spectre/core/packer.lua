local M = {}
local fn = vim.fn

local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
local compile_path = install_path .. "/plugin/packer_compiled.lua"

-- Clone Packer
M.bootstrap = function()
  vim.api.nvim_set_hl(0, "NormalFloat", { bg = "#1e222a" })

  if fn.empty(fn.glob(install_path)) > 0 then
    print "Cloning packer..."
    packer_bootstrap = fn.system {
      "git",
      "clone",
      "--depth",
      "1",
      "https://github.com/wbthomason/packer.nvim",
      install_path
    }
    vim.cmd [[packadd packer.nvim]]
    require("spectre.plugins")
    vim.cmd [[PackerSync]]
  end
end

M.options = {
  auto_clean = true,
  compile_on_sync = true,
  compile_path = compile_path,
  git = { clone_timeout = 6000 },
  display = {
    open_fn = function()
      return require("packer.util").float { border = "single" }
    end,
  },
}

M.load_plugins = function(plugins)
  local present, packer = pcall(require, "packer")

  if not present then
    return
  end

  packer.init(M.options)

  packer.startup(function(use)
    for key, v in pairs(plugins) do
      plugins[key][1] = key
      use(v)
    end
  end)
end

return M
