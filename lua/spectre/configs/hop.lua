local status_ok, hop = pcall(require, "hop")
local map = require("spectre.core.utils").map

if not status_ok then
  return
end

local opts = { noremap = true, silent = true }

hop.setup({
  keys = 'etovxqpdygfblzhckisuran'
})

map('n', '<leader>hw', hop.hint_words, opts)
map("n", "q", ":HopChar2<CR>", opts)
