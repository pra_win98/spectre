local M = {}
local merge = require("spectre.core.utils").merge
local map = require("spectre.core.utils").map

M.setup = function(_, bufnr)
  local default_opts = { noremap = true, silent = true, buffer = bufnr }

  local mappings = {
    n = {
      ["gD"] = { "<cmd>lua vim.lsp.buf.declaration()<CR>" },
      ["gd"] = { "<cmd>lua vim.lsp.buf.definition()<CR>" },
      ["K"] = { "<cmd>lua vim.lsp.buf.hover()<CR>" },
      ["gi"] = { "<cmd>lua vim.lsp.buf.implementation()<CR>" },
      ["<C-k>"] = { "<cmd>lua vim.lsp.buf.signature_help()<CR>" },
      ["gr"] = { "<cmd>lua vim.lsp.buf.references()<CR>" },
      ["[d"] = { '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>' },
      ["gl"] = { '<cmd>lua vim.diagnostic.open_float({ border = "rounded" })<CR>' },
      ["]d"] = { '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>' },
      ["<leader>q"] = { "<cmd>lua vim.diagnostic.setloclist()<CR>" },
      ["<leader>rn"] = { require('spectre.ui.renamer').open }
    }
  }

  for mode, mode_mapping in pairs(mappings) do
    for lhs, options in pairs(mode_mapping) do
      local rhs, opts = unpack(options)
      opts = merge(default_opts, opts or {})
      map(mode, lhs, rhs, opts)
    end
  end
end

return M
