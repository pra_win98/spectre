local status_ok, telescope = pcall(require, "telescope")
local map = require('spectre.core.utils').map
if not status_ok then
  return
end

local actions = require("telescope.actions")
local previewers = require("telescope.previewers")
local builtin = require("telescope.builtin")
local sorters = require("telescope.sorters")

telescope.setup {
  defaults = {
    layout_config = {
      horizontal = { mirror = false },
      vertical = { mirror = false },
      prompt_position = "top"
    },
    file_sorter = sorters.get_fzy_sorter,
    prompt_prefix = " 🔍 ",
    color_devicons = true,
    sorting_strategy = "ascending",
    file_previewer = previewers.vim_buffer_cat.new,
    grep_previewer = previewers.vim_buffer_vimgrep.new,
    qflist_previewer = previewers.vim_buffer_qflist.new,
    file_ignore_patterns = {
      "node_modules", ".git/*", ".next"
    },
    mappings = {
      i = {
        ["<C-x>"] = false,
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-q>"] = actions.send_to_qflist,
        ["<C-s>"] = actions.cycle_previewers_next,
        ["<C-a>"] = actions.cycle_previewers_prev,
      },
      n = {
        ["<C-s>"] = actions.cycle_previewers_next,
        ["<C-a>"] = actions.cycle_previewers_prev,
        ["<S-p>"] = builtin.live_grep,
      }
    }
  }
}
local function project_files()
  local opts = {} -- define here if you want to define something
  local ok = pcall(require "telescope.builtin".git_files, opts)
  if not ok then require "telescope.builtin".find_files(opts) end
end

local opts = { noremap = true, silent = true }
map("n", "<C-p>", project_files, opts)
