local M = {}
local space = " "
local icons = require("spectre.ui.icons")

M.fileType = function()

end

M.fileInfo = function()
  local fileName = (vim.fn.expand "%" == "" and "Untitled ") or vim.fn.expand "%:t"
  local modified = vim.api.nvim_eval_statusline("%M", {}).str == "+" and "%#DiagnosticSignError#" .. " [+]" or ""
  if fileName ~= "Untitled " then
    local ext = vim.fn.fnamemodify(fileName, ":e")
    local icon, color = require("nvim-web-devicons").get_icon_color(fileName, ext)
    if icon then
      -- local table = vim.api.nvim_get_hl_by_name("statusline", true)
      -- vim.api.nvim_set_hl(0, "FileIcon", { bg = table["background"], fg = color, cterm = { bold = true } })
      vim.api.nvim_set_hl(0, "FileIcon", { fg = color, cterm = { bold = true } })
      return "%#FileIcon#" .. icon .. space .. fileName .. modified
    end
  end
  return fileName
end
M.cursor_position = function()
  local current_line = vim.fn.line "."
  local total_line = vim.fn.line "$"
  local text = math.modf((current_line / total_line) * 100) .. tostring "%%"

  local cp = vim.api.nvim_win_get_cursor(vim.api.nvim_get_current_win())
  text = (current_line == 1 and "Top") or text
  text = (current_line == total_line and "Bot") or text

  return "Ln " ..
      cp[1] ..
      ",Col " ..
      cp[2] ..
      space .. '[' .. current_line .. ":" .. total_line .. ']' .. space .. "%#DiagnosticSignInfo#" .. text .. space
end

M.scroll = function()
  local current_line = vim.fn.line "."
  local total_line = vim.fn.line "$"

  local chars = {
    "_",
    "▁",
    "▂",
    "▃",
    "▄",
    "▅",
    "▆",
    "▇",
    "█",
  }
  local index = 1
  if current_line == 1 then
    index = 1
  elseif current_line == total_line then
    index = #chars
  else
    local line_of_fraction = vim.fn.floor(current_line) / vim.fn.floor(total_line)
    index = vim.fn.float2nr(line_of_fraction * #chars)
    if index == 0 then
      index = 1
    end
  end
  return "%#DiagnosticSignWarn#" .. chars[index]
end
M.git = function()
  if not vim.b.gitsigns_head or vim.b.gitsigns_git_status then
    return ""
  end

  local git_status = vim.b.gitsigns_status_dict

  local added = (git_status.added and git_status.added ~= 0) and (icons.git.Add .. space .. git_status.added .. space) or
      ""
  local changed = (git_status.changed and git_status.changed ~= 0) and
      (icons.git.Mod .. space .. git_status.changed .. space) or
      ""
  local removed = (git_status.removed and git_status.removed ~= 0) and
      (icons.git.Remove .. space .. git_status.removed .. space)
      or ""
  local branch_name = icons.git.Octoface .. space .. git_status.head .. " "

  return space .. branch_name .. added .. changed .. removed .. space
end

M.LSP_Diagnostics = function()
  if not rawget(vim, "lsp") then
    return ""
  end

  local errors = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.ERROR })
  local warnings = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.WARN })
  local hints = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.HINT })
  local info = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.INFO })

  local showerrors = (errors and errors > 0) and ("%#DiagnosticSignError#" .. icons.diagnostics.Error .. errors .. " ")
      or ""
  local showwarnings = (warnings and warnings > 0) and
      ("%#DiagnosticSignWarn#" .. icons.diagnostics.Warn .. warnings .. " ") or ""
  local showhints = (hints and hints > 0) and ("%#DiagnosticSignHint#" .. icons.diagnostics.Hint .. hints .. " ") or ""
  local showinfo = (info and info > 0) and ("%#DiagnosticSignInfo#" .. icons.diagnostics.Info .. info .. " ") or ""

  return showerrors .. showwarnings .. showhints .. showinfo
end

M.statusline = function()
  return table.concat {
    M.fileInfo(),
    M.git(),
    M.LSP_Diagnostics(),
    "%=",
    M.cursor_position(),
    M.scroll()
  }
end

M.setup = function()
  vim.o.statusline = "%{%v:lua.require'spectre.ui.statusline'.statusline()%}"
end

return M;
